
from asyncua import Server, ua
import asyncio

class PLCSimulator:
    def __init__(self):
       # Initialize digital and alarm registers. Complete the registers with the needed signals 
        self.digital_inputs = {"DI0": False, "DI1": False, "DI2": False, "DI3": False,
                              "DI4": False, "DI5": False, "DI6": False, "DI7": False,
                              "DI8": False, "DI9": False}
        self.analog_inputs = {"AI0": 47.0}
        self.digital_outputs = {"DQ0": False, "DQ1": False, "DQ2": False, "DQ3": False}
        self.alarms = {
           "A0": {"Active": False, "UnAck": False, "Status": False},
           "A1": {"Active": False, "UnAck": False, "Status": False},
           "A2": {"Active": False, "UnAck": False, "Status": False},
           "A3": {"Active": False, "UnAck": False, "Status": False},
           "A4": {"Active": False, "UnAck": False, "Status": False},
           "A5": {"Active": False, "UnAck": False, "Status": False}
       }
    
        
        # Time interval for cyclic execution (in seconds)
        self.cycle_time = 0.2
        self.init_required = False
        # Initialize myobj as None
        self.myobj = None
        
        
    async def update_inputs(self):
     try:
        for name in self.digital_inputs.keys():
            myvar = await self.myobj.get_child(f"{self.idx}:{name}")
            value = await myvar.read_value()
            self.digital_inputs[name] = bool(value)  # Update the value for the specific key
        
        for name in self.analog_inputs.keys():
                myvar = await self.myobj.get_child(f"{self.idx}:{name}")
                value = await myvar.read_value()
                self.analog_inputs[name] = float(value)
                
     except Exception as e:
            print(f"Error updating inputs: {e}")

    
    async def write_outputs(self):
     try:
          for name, value in self.digital_outputs.items():
             myvar = await self.myobj.get_child(f"{self.idx}:{name}")
             await myvar.write_value(value)
     except Exception as e:
        print(f"Error updating outputs: {e}")

    async def set_alarms(self):
        for key, values in self.alarms.items():
            myalarm = await self.myobj.get_child(f"{self.idx}:{key}")
            if values["Active"]:
                values["UnAck"] = True
                values["Status"] = True
            for newkey, value in values.items():
                myvar = await myalarm.get_child(f"{self.idx}:{newkey}")
                await myvar.write_value(value)

    async def reset_alarms(self):
        for key in self.alarms.keys():
            self.alarms[key].update({"UnAck":False})
            self.alarms[key].update({"Status":False})

    
    async def execute_control_logic(self):
        # Initialize variables as needed
        self.digital_outputs = {"DQ0": False, "DQ1": False, "DQ2": False, "DQ3": False}
        try:
            while True:
                # Updating inputs from server
                await self.update_inputs()

                # Implement alarm logic
                # (Example) Tank level too high
                self.alarms["A0"]["Active"] = self.digital_inputs["DI8"]
                self.alarms["A1"]["Active"] = self.digital_inputs["DI5"]
                self.alarms["A2"]["Active"] = self.analog_inputs["AI0"] >= 80.0
                self.alarms["A3"]["Active"] = self.analog_inputs["AI0"] <= 10.0
                self.alarms["A4"]["Active"] = self.digital_inputs["DI9"]
                self.alarms["A5"]["Active"] = self.digital_inputs["DI3"] 
                # Add more alarms here...

                # Press RESET for ack.
                if self.digital_inputs["DI4"]:
                    await self.reset_alarms()
                        
                # Setting alarms on server
                await self.set_alarms()

                # Implement GRAFCET logic

                # PREFILL step
                if self.digital_inputs["DI0"]:
                  if self.digital_inputs["DI6"]:
                      # Transition to Run Step
                      self.digital_outputs["DQ0"] = False

                  else:
                      # Continue PREFILL actions
                      self.digital_outputs["DQ0"] = True  # Open the valve for filling
                      self.digital_outputs["DQ1"] = False  # Close discharging valve

              #  RUN step
                if self.digital_inputs["DI1"] and self.digital_inputs["DI7"]:
                  # Fill the tank until the high level is reached (DI7) and then heat
                      self.digital_outputs["DQ2"] = True
                      self.digital_outputs["DQ0"] = False
                     
                      if self.analog_inputs["AI0"] > 45.0:
                          self.digital_outputs["DQ0"] = False
                          self.digital_outputs["DQ1"] = True  # Open the valve for discharging
                          self.digital_outputs["DQ2"] = False
                  
                      if self.digital_inputs["DI6"]:
                         self.digital_outputs["DQ0"] = True  # Open valve for fill
                         self.digital_outputs["DQ1"] = False  # Open the valve for discharging
                         self.digital_outputs["DQ2"] = False  # Close the valve for heating
                      
                     
                 
                elif self.digital_inputs["DI2"]:
                   #Stop Button
                    self.digital_outputs["DQ0"] = False  # Stop fluid inflow
                    self.digital_outputs["DQ2"] = False  # Stop heating
                    self.digital_outputs["DQ1"] = False  # Stop discharge
                    self.digital_outputs["DQ3"] = False   # Close discharge gate


                # ALARMS
                elif self.alarms["A1"]["Active"]:
                    # Tank Level Too Low Alarm (A1)
                    self.digital_outputs["DQ0"] = False  # Stop fluid inflow
                    self.digital_outputs["DQ2"] = False  # Stop heating
                    self.digital_outputs["DQ1"] = False  # Stop discharge
                    self.digital_outputs["DQ3"] = False   # Close discharge gate
                   
                    await self.reset_alarms()  # Acknowledge the alarm


                elif self.alarms["A0"]["Active"]:
                    # Tank Level Too High Alarm (A0)
                    self.digital_outputs["DQ0"] = False  # Stop fluid inflow
                    self.digital_outputs["DQ2"] = False  # Stop heating
                    self.digital_outputs["DQ1"] = False  # Stop discharge
                    self.digital_outputs["DQ3"] = True   # Open discharge gate
                    
                    await self.reset_alarms()  # Acknowledge the alarm

                elif self.alarms["A5"]["Active"]:
                    # Emergency Button Pressed Alarm (A5)
                    self.digital_outputs["DQ0"] = False  # Stop fluid inflow
                    self.digital_outputs["DQ2"] = False  # Stop heating
                    self.digital_outputs["DQ1"] = False  # Stop discharge
                    self.digital_outputs["DQ3"] = True   # Open discharge gate
                    
                    await self.reset_alarms()  # Acknowledge the alarm
                
                # DISCHARGING DOOR OPEN ALARM
                elif self.alarms["A4"]["Active"]:
                    # Discharging Door Open Alarm (A4)
                    self.digital_outputs["DQ0"] = False  # Stop fluid inflow
                    self.digital_outputs["DQ2"] = False  # Stop heating
                    self.digital_outputs["DQ1"] = False  # Stop discharge
                    self.digital_outputs["DQ3"] = False  # Close discharge gate
                   
                    await self.reset_alarms()  # Acknowledge the alarm

                # ERROR step
                elif any([alarm["Active"] for alarm in self.alarms.values()]):
                  active_alarms = [key for key, alarm in self.alarms.items() if alarm["Active"]]
                  if any(active_alarms):
                     print(f"Error: Alarm condition detected. Active alarms: {active_alarms}")

               # Acknowledge the alarms
                  for alarm in self.alarms.values():
                     alarm["UnAck"] = True
                  await self.reset_alarms()


                # RESET step
                elif self.digital_inputs["DI4"]:
                    # Acknowledge the alarms
                    print("Resetting alarms.")
                    await self.reset_alarms()

                
                # Setting outputs on server
                await self.write_outputs()
                # Sleeping for cycle time
                await asyncio.sleep(self.cycle_time)
        finally:
            await self.server.stop()
            print("Stopping server")

    
    async def set_opcua_server(self):
        self.server = Server()
        await self.server.init() 
        self.server.set_endpoint("opc.tcp://localhost:7000/freeopcua/server/")

        # setup our own namespace, not really necessary but should as spec
        uri = "http://examples.freeopcua.github.io"
        self.idx = await self.server.register_namespace(uri)

        # get Objects node, this is where we should put our nodes
        objects = self.server.get_objects_node()

        # populating our address space
        self.myobj = await objects.add_object(self.idx, "myPLC")
        
        for key, value in self.digital_inputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
        for key, value in self.analog_inputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
        for key, value in self.digital_outputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
        for key, values in self.alarms.items():
            myalarm = await self.myobj.add_object(self.idx, key)
            for newkey, value in values.items():
                myvar = await myalarm.add_variable(self.idx, newkey, value)
                await myvar.set_writable()

        # starting!
        await self.server.start()
        print("Server started")

    async def stop(self):
        await self.server.stop()
        
    
    async def main(self):
        # Set OPC UA server
        await self.set_opcua_server()

        # Execute cycle program
        await self.execute_control_logic()

if __name__ == "__main__":
    plc = PLCSimulator()
    try:
        asyncio.run(plc.main())
    except KeyboardInterrupt:
        print("PLC stopped")



