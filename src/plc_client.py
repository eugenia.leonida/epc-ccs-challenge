#%gui asyncio
from asyncua import Client
import asyncio 

class PLCClient:

    def __init__(self, url, timeout):
        self.client = Client(url, timeout)
        self.myobj = None
        self.idx = None

    async def set_object_value(self, name, value):
        myvar = await self.myobj.get_child(f"{self.idx}:{name}")
        await myvar.write_value(value)

    async def get_object_value(self, name):
        myvar = await self.myobj.get_child(f"{self.idx}:{name}")
        value = await myvar.read_value()
        return value

    async def get_alarm_status(self, name):
        myalarm = await self.myobj.get_child(f"{self.idx}:{name}")
        myvar = await myalarm.get_child(f"{self.idx}:Status")
        value = await myvar.read_value()
        return value

    async def set_object_pulse(self, name):
        myvar = await self.myobj.get_child(f"{self.idx}:{name}")
        print(f"Setting pulse for {name}")
        actual_value_before = await myvar.read_value()
        print(f"Actual value of {name} before setting pulse: {actual_value_before}")

        await myvar.write_value(True)
        await asyncio.sleep(0.5)
        await myvar.write_value(False)

        actual_value_after = await myvar.read_value()
        print(f"Actual value of {name} after setting pulse: {actual_value_after}")

    async def disconnect(self):
        await self.client.disconnect()

    async def init(self):
        try:
            print("Connecting to the OPC UA server...")
            await self.client.connect()
            print("Connected to the OPC UA server")

            namespace_uri = "http://examples.freeopcua.github.io"
            self.idx = await self.client.get_namespace_index(namespace_uri)
            print("Namespace index:", self.idx)

            self.myobj = await self.client.nodes.root.get_child(["0:Objects", f"{self.idx}:myPLC"])
            print("Successfully initialized")

        except Exception as e:
            print(f"Error initializing: {e}")

if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    try:
        url = "opc.tcp://localhost:7000/freeopcua/server/"
        timeout = 5.0
        loop.run_until_complete(PLCClient(url, timeout).init())
    except Exception as e:
        print(f"Error during execution: {e}")
