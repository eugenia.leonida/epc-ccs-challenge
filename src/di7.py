
from asyncua import Server, ua
import asyncio

class PLCSimulator:
    def __init__(self):
       # Initialize digital and alarm registers. Complete the registers with the needed signals 
        self.digital_inputs = { "DI1":False, "DI7": False}
        self.analog_inputs = {"AI0": 47.0}
        self.digital_outputs = {"DQ0": False, "DQ1": False, "DQ2": False, "DQ3": False}
    
        
        # Time interval for cyclic execution (in seconds)
        self.cycle_time = 0.2
        self.init_required = False
        # Initialize myobj as None
        self.myobj = None
        
        
    async def update_inputs(self):
     try:
        for name in self.digital_inputs.keys():
            myvar = await self.myobj.get_child(f"{self.idx}:{name}")
            value = await myvar.read_value()
            self.digital_inputs[name] = bool(value)  # Update the value for the specific key
        
        for name in self.analog_inputs.keys():
                myvar = await self.myobj.get_child(f"{self.idx}:{name}")
                value = await myvar.read_value()
                self.analog_inputs[name] = float(value)
                
     except Exception as e:
            print(f"Error updating inputs: {e}")

    
    async def write_outputs(self):
     try:
          for name, value in self.digital_outputs.items():
             myvar = await self.myobj.get_child(f"{self.idx}:{name}")
             await myvar.write_value(value)
     except Exception as e:
        print(f"Error updating outputs: {e}")

    
    async def execute_control_logic(self):
        # Initialize variables as needed
        self.digital_outputs = {"DQ0": False, "DQ1": False, "DQ2": False, "DQ3": False}
        try:
            while True:
                # Updating inputs from server
                await self.update_inputs()

                # Implement alarm logic
                
                # Press RESET for ack.
               
                # Setting alarms on server
               
                # Implement GRAFCET logic

                # PREFILL step
                # GRAFCET logic for PREFILL step
               
              # GRAFCET logic for RUN step
                if self.digital_inputs["DI1"]:
                  # Continue filling the tank until the high level is reached (DI7)
                  # Transition to Tank Filling Step when the high level is reached
                  if self.digital_inputs["DI7"]:
                      self.digital_outputs = {"DQ2": True, "DQ0": False}
                     
                      if self.analog_inputs["AI0"] > 45.0:
                          self.digital_outputs = {"DQ0": False, "DQ1": True, "DQ2": False}
     
                # Setting outputs on server
                await self.write_outputs()
                # Sleeping for cycle time
                await asyncio.sleep(self.cycle_time)
        finally:
            await self.server.stop()
            print("Stopping server")

    
    async def set_opcua_server(self):
        self.server = Server()
        await self.server.init() 
        self.server.set_endpoint("opc.tcp://localhost:7000/freeopcua/server/")

        # setup our own namespace, not really necessary but should as spec
        uri = "http://examples.freeopcua.github.io"
        self.idx = await self.server.register_namespace(uri)

        # get Objects node, this is where we should put our nodes
        objects = self.server.get_objects_node()

        # populating our address space
        self.myobj = await objects.add_object(self.idx, "myPLC")
        
        for key, value in self.digital_inputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
        for key, value in self.analog_inputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
        for key, value in self.digital_outputs.items():
            myvar = await self.myobj.add_variable(self.idx, key, value)
            await myvar.set_writable()
       

        # starting!
        await self.server.start()
        print("Server started")

    async def stop(self):
        await self.server.stop()
        
    
    async def main(self):
        # Set OPC UA server
        await self.set_opcua_server()

        # Execute cycle program
        await self.execute_control_logic()

if __name__ == "__main__":
    plc = PLCSimulator()
    try:
        asyncio.run(plc.main())
    except KeyboardInterrupt:
        print("PLC stopped")



