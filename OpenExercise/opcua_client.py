from opcua import Client
import time
import random

class TankProcessSimulator:
    def __init__(self, server_endpoint):
        # Create an OPC UA client
        self.client = Client(server_endpoint)

    def connect_to_server(self):
        # Connect to the OPC UA server
        self.client.connect()
        print("Connected to OPC UA server")

    def simulate_process(self):
        try:
            while True:
                # Simulate random data for tank variables
                tank_level = random.uniform(0, 100)
                temperature = random.uniform(20, 80)
                analog_input = random.uniform(0, 100)

                # Write simulated data to OPC UA server
                self.client.get_node("ns=2;i=2").set_value(tank_level)  # TankLevel
                self.client.get_node("ns=2;i=3").set_value(temperature)  # Temperature
                self.client.get_node("ns=2;i=11").set_value(analog_input)  # Analog Input

                print(f"Simulated Data - Tank Level: {tank_level}, Temperature: {temperature}, Analog Input: {analog_input}")

                # Simulate a delay to represent the cycle time
                time.sleep(0.2)

                # Additional print statements to indicate the progress of the simulation
                print("Tank process simulation: Writing simulated data to OPC UA server.")
                print("...")

        except KeyboardInterrupt:
            pass

    def disconnect_from_server(self):
        # Disconnect from the OPC UA server
        self.client.disconnect()
        print("Disconnected from OPC UA server")

if __name__ == "__main__":
    # Replace 'opc.tcp://<your_server_ip>:4840/freeopcua/server/' with the actual server endpoint
    server_endpoint = "opc.tcp://localhost:4840/freeopcua/server/"
    
    # Create a TankProcessSimulator instance and connect to the server
    simulator = TankProcessSimulator(server_endpoint)
    simulator.connect_to_server()

    try:
        # Simulate the tank process
        simulator.simulate_process()

    finally:
        # Disconnect from the server on program exit
        simulator.disconnect_from_server()
