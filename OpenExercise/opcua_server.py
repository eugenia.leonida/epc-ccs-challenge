from opcua import Server
import time
import random

class TankControlServer:
    def __init__(self):
        # Initialize the server
        self.server = Server()

        # Set the endpoint for the server
        self.server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")

        # Setup server namespace
        uri = "http://example.org"
        self.idx = self.server.register_namespace(uri)

        # Create a new object in the server
        tank_object = self.server.nodes.objects.add_object(self.idx, "TankControlSystem")

        # Add variables to the object
        self.temperature = tank_object.add_variable(self.idx, "Temperature", 0.0)
        self.tank_level = tank_object.add_variable(self.idx, "TankLevel", 0)

        # Alarm Definitions
        self.alarms = {
            "A0": {"Active": False, "Description": "Tank Level Too High Alarm", "Acknowledged": False},
            "A1": {"Active": False, "Description": "Tank Level Too Low Alarm", "Acknowledged": False},
            "A2": {"Active": False, "Description": "Fluid Temperature Too High Alarm", "Acknowledged": False},
            "A3": {"Active": False, "Description": "Fluid Temperature Too Low Alarm", "Acknowledged": False},
            "A4": {"Active": False, "Description": "Discharging Door Open Alarm", "Acknowledged": False},
            "A5": {"Active": False, "Description": "Emergency Button Pressed Alarm", "Acknowledged": False},
        }
        
        # Digital Outputs
        self.dq0 = tank_object.add_variable(self.idx, "DQ0", False)
        self.dq1 = tank_object.add_variable(self.idx, "DQ1", False)
        self.dq2 = tank_object.add_variable(self.idx, "DQ2", False)
        self.dq3 = tank_object.add_variable(self.idx, "DQ3", False)

        # Digital Inputs
        self.start_button = tank_object.add_variable(self.idx, "DI0", False)
        self.run_button = tank_object.add_variable(self.idx, "DI1", False)
        self.stop_button = tank_object.add_variable(self.idx, "DI2", False)
        self.emergency_button = tank_object.add_variable(self.idx, "DI3", False)
        self.reset_button = tank_object.add_variable(self.idx, "DI4", False)
        self.low_low = tank_object.add_variable(self.idx, "DI5", False)
        self.low = tank_object.add_variable(self.idx, "DI6", False)
        self.high = tank_object.add_variable(self.idx, "DI7", False)
        self.high_high = tank_object.add_variable(self.idx, "DI8", False)
        self.gate_status = tank_object.add_variable(self.idx, "DI9", False)

        # Analog Input
        self.analog_input = tank_object.add_variable(self.idx, "AI0", 0.0)

        # Initialize variables for control logic
        self.tank_filled = False
        self.fluid_heated = False
        self.prefill_complete = False
        self.error_mode = False
        self.alarms_acknowledged = False

    def start_server(self):
        # Start the server
        self.server.start()

        try:
           while True:
               # Update variable values (simulated data)
               self.tank_level.set_value(random.uniform(0, 100))
               self.temperature.set_value(random.uniform(20, 80))
               self.analog_input.set_value(random.uniform(0, 100))

               # Implement your control logic here
               self.control_logic()

               # Simulate a delay to represent the cycle time
               time.sleep(0.2)

        except KeyboardInterrupt:
           pass
        finally:
           # Stop the server on program exit
           self.server.stop()
           self.server.halt()
            
    def reset_alarms(self):
        # Reset all alarms
        for key in self.alarms:
            self.alarms[key]["Active"] = False
            self.alarms[key]["Acknowledged"] = False

    def trigger_alarm(self, alarm_key):
        # Trigger a specific alarm
        self.alarms[alarm_key]["Active"] = True

    def acknowledge_alarm(self, alarm_key):
        # Acknowledge a specific alarm
        self.alarms[alarm_key]["Acknowledged"] = True

    def control_logic(self):
        try:
            # Initialization Step (INIT)
            if not self.prefill_complete and not self.alarms_acknowledged:
                # Reset all variables and flags
                self.tank_filled = False
                self.fluid_heated = False
                self.error_mode = False

                # Ensure all valves are closed
                self.dq0.set_value(False)
                self.dq1.set_value(False)
                self.dq2.set_value(False)
                self.dq3.set_value(False)

                # Reset alarms
                self.reset_alarms()

                # Set the system to the initial state
                self.prefill_complete = False
                self.alarms_acknowledged = False


            # Prefilling Step (PREFILL)
            elif not self.prefill_complete:
                if self.start_button.get_value() and not self.gate_status.get_value():
                    # Conditions: Start Button Pressed and Discharging Gate Closed
                    self.dq0.set_value(True)  # Open fluid inflow
                    
                    while not self.low.get_value():
                              time.sleep(0.2)

                    # Prefill the tank until the low level is reached
                    if self.low.get_value()  == True:
                        self.dq0.set_value(False)  # Stop fluid inflow
                        self.prefill_complete = True

            # Run Step (RUN)
            elif self.run_button.get_value() and self.prefill_complete:
                if not self.fluid_heated and not any(self.alarms[key].get("Active") for key in self.alarms):
                    # Conditions: Run Button Pressed and No Active Alarms
                    self.dq0.set_value(True)  # Start tank filling
    

                    # Transition to Tank Filling Step when the high level is reached
                    if self.high.get_value():
                        self.dq0.set_value(False)
                        self.tank_filled = True

            # Tank Filling Step (FILL)
                if self.tank_filled and not self.fluid_heated:
                    if self.high.get_value():
                     # Conditions: Tank level is high
                     # Actions: Close the valve for filling
                       self.dq0.set_value(False)
                       self.dq2.set_value(True)
                       self.fluid_heated = True

            # Temperature Control Step (HEAT)
                if self.tank_filled and self.fluid_heated:
                    # Monitor the temperature
                    # Transition to Tank Discharging Step when the temperature reaches 45°C
                    if self.analog_input.get_value() >= 45:
                        self.dq2.set_value(False)
                        self.dq1.set_value(True)
                    
                    if self.low.get_value():
                      # Conditions: Tank level is low
                      # Actions: Close the valve for heating, open the valve for discharging
                       self.dq2.set_value(False)
                       self.dq1.set_value(False)
                       self.tank_filled = False

            # Stop Step (STOP)
            elif not self.stop_button.get_value():
                # Conditions: Stop Button Pressed
                # Actions: Stop all processes, set variables and flags to initial state
                self.dq0.set_value(False)  # Stop fluid inflow
                self.dq2.set_value(False)  # Stop heating
                self.dq1.set_value(False)  # Stop discharge
                self.tank_filled = False
                self.fluid_heated = False
                self.prefill_complete = False
                self.alarms_acknowledged = False

            # Emergency Stop Step (ESTOP)
            elif self.emergency_button.get_value():
                # Conditions: Emergency Button Pressed
                # Actions: Immediate system shutdown, open the tank, full discharge
                self.dq0.set_value(False)  # Stop fluid inflow
                self.dq2.set_value(False)  # Stop heating
                self.dq1.set_value(False)  # Stop discharge
                self.dq3.set_value(True)  # Open discharging gate
                self.error_mode = True

                # Transition to Error Mode Step
                self.alarms_acknowledged = True
                self.reset_alarms()

            # Error Mode Step (ERROR)
            elif self.error_mode:
                # Conditions: No active alarms
                # Actions: Reset alarms, close discharging gate
                if not any(self.alarms[key].get("Active") for key in self.alarms):
                    self.reset_alarms()
                    self.dq3.set_value(False)  # Close discharging gate
                    self.error_mode = False

         # Handle alarms
            self.handle_alarms()
            
        except Exception as e:
            print(f"Error in control logic: {e}")
    
    def handle_alarms(self):
        # Handle active alarms
        for key, alarm in self.alarms.items():
            if alarm["Active"] and not alarm["Acknowledged"]:
                # Take appropriate actions for each alarm
                if key == "A0":
                    # Tank Level Too High Alarm
                    self.handle_tank_level_too_high_alarm()
                elif key == "A1":
                    # Tank Level Too Low Alarm
                    self.handle_tank_level_too_low_alarm()
                elif key == "A2":
                    # Fluid Temperature Too High Alarm
                    self.handle_fluid_temperature_too_high_alarm()
                elif key == "A3":
                    # Fluid Temperature Too Low Alarm
                    self.handle_fluid_temperature_too_low_alarm()
                elif key == "A4":
                    # Discharging Door Open Alarm
                    self.handle_discharging_door_open_alarm()
                elif key == "A5":
                    # Emergency Button Pressed Alarm
                    self.handle_emergency_button_pressed_alarm()

    # Define methods for handling each specific alarm
    def handle_tank_level_too_high_alarm(self):
        # Implementation for Tank Level Too High Alarm
        # Example: Immediate system shutdown, full discharge, open the tank door
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.dq3.set_value(True)   # Open discharging gate
        self.error_mode = True

    def handle_tank_level_too_low_alarm(self):
        # Implementation for Tank Level Too Low Alarm
        # Example: Immediate system shutdown, full discharge
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.dq3.set_value(True)   # Open discharging gate
        self.error_mode = True

    def handle_fluid_temperature_too_high_alarm(self):
        # Implementation for Fluid Temperature Too High Alarm
        # Example: Shutdown heating system, full discharge
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.dq3.set_value(True)   # Open discharging gate
        self.error_mode = True

    def handle_fluid_temperature_too_low_alarm(self):
        # Implementation for Fluid Temperature Too Low Alarm
        # Example: Stop fluid flow, full discharge
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.error_mode = True

    def handle_discharging_door_open_alarm(self):
        # Implementation for Discharging Door Open Alarm
        # Example: Immediate system shutdown, full discharge
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.dq3.set_value(True)   # Open discharging gate
        self.error_mode = True

    def handle_emergency_button_pressed_alarm(self):
        # Implementation for Emergency Button Pressed Alarm
        # Example: Immediate system shutdown, full discharge
        self.dq0.set_value(False)  # Stop fluid inflow
        self.dq2.set_value(False)  # Stop heating
        self.dq1.set_value(False)  # Stop discharge
        self.dq3.set_value(True)   # Open discharging gate
        self.error_mode = True

        # Transition to Error Mode Step
        self.alarms_acknowledged = True
        self.reset_alarms()

if __name__ == "__main__":
    tank_server = TankControlServer()
    tank_server.start_server()
