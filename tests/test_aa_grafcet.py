import asyncio
import pytest
import pytest_asyncio

from src.plc_client import PLCClient

SERVER_URL = "opc.tcp://localhost:7000/freeopcua/server/"
CLIENT_TIMEOUT = 5  # seconds

@pytest_asyncio.fixture()
async def plc() -> PLCClient:
    """Instance of the OPC UA client to communicate with the simulator."""
    plc = PLCClient(url=SERVER_URL, timeout=CLIENT_TIMEOUT)
    await plc.init()
    yield plc
    await plc.disconnect()

# Press START button and check that tank is filling
@pytest.mark.asyncio
async def test_start_prefilling(plc: PLCClient):
    assert await plc.get_object_value("DQ0") == False  # Tank is not filling
    assert await plc.get_object_value("DQ1") == False  # Tank is not discharging
    assert await plc.get_object_value("DQ2") == False  # Liquid is not heating

    await plc.set_object_pulse("DI0")  # Press START button
    await asyncio.sleep(1)  # Waiting for transition into the next step
    print("DQ0:", await plc.get_object_value("DQ0"))
    
    assert await plc.get_object_value("DQ0") == True  # Tank is filling
    assert await plc.get_object_value("DQ1") == False  # Tank is not discharging
    assert await plc.get_object_value("DQ2") == False  # Liquid is not heating

# Test RUN button pressed and low level
@pytest.mark.asyncio
async def test_run_button_pressed(plc: PLCClient):
    # Simulate pressing the RUN button
    await plc.set_object_pulse("DI1")  # Press RUN button
    await plc.set_object_pulse("DI6") 
    await asyncio.sleep(1) 
    
    print("DQ0:", await plc.get_object_value("DQ0"))


    assert await plc.get_object_value("DQ0") == True  # Tank filling continues
    assert await plc.get_object_value("DQ2") == False  # Liquid is heating

# Test high level has been reached
@pytest.mark.asyncio
async def test_high_level_reached(plc: PLCClient):
    # Simulate reaching the high level
    await plc.set_object_pulse("DI1")  
    await plc.set_object_pulse("DI7")  
    await asyncio.sleep(1)
    
    print("DQ0:", await plc.get_object_value("DQ0"))
    assert await plc.get_object_value("DQ0") == False  # Tank filling stops 
    assert await plc.get_object_value("DQ2") == True  # Tank starts heating

# Test setpoint has been reached
@pytest.mark.asyncio
async def test_setpoint_reached(plc: PLCClient):
    # Simulate reaching the setpoint 
    await plc.set_object_pulse("DI1")  
    await plc.set_object_pulse("DI7")  
    await plc.set_object_value("AI0", 48.0)  # Set analog input AI0 

    await asyncio.sleep(1)
    
    print("DQ0:", await plc.get_object_value("DQ0"))
    assert await plc.get_object_value("DQ0") == False  # Tank filling stops
    assert await plc.get_object_value("DQ1") == True  # Tank starts discharging

# Test STOP button
@pytest.mark.asyncio
async def test_stop_button_pressed(plc: PLCClient):
    # Simulate pressing the STOP button
    await plc.set_object_pulse("DI2")  # Press STOP button
    await asyncio.sleep(1)  

    print("DQ0:", await plc.get_object_value("DQ0"))
    assert await plc.get_object_value("DQ0") == False  # Tank filling stops
    assert await plc.get_object_value("DQ1") == False  # Tank stops discharging
    assert await plc.get_object_value("DQ2") == False  # Liquid heating stops
    assert await plc.get_object_value("DQ3") == False  # Discharge gate closes


# Test ESTOP button
@pytest.mark.asyncio
async def test_emergency_stop_button(plc: PLCClient):
    # Simulate pressing the emergency stop button
    await plc.set_object_pulse("DI3")  # Press Emergency Stop button
    await asyncio.sleep(1) 

    assert await plc.get_object_value("DQ0") == False  # Tank filling stops
    assert await plc.get_object_value("DQ1") == False  # Tank stops discharging
    assert await plc.get_object_value("DQ2") == False  # Liquid heating stops
    assert await plc.get_object_value("DQ3") == True  # Discharge gate opens