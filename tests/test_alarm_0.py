import asyncio
import pytest
import pytest_asyncio

from src.plc_client import PLCClient

SERVER_URL = "opc.tcp://localhost:7000/freeopcua/server/"
CLIENT_TIMEOUT = 5  # seconds

@pytest_asyncio.fixture()
async def plc() -> PLCClient:
    """ Instance of the OPC UA client to communicate with the simulator """
    plc = PLCClient(url=SERVER_URL, timeout=CLIENT_TIMEOUT)
    await plc.init()
    
    # Set initial conditions
    await plc.set_object_value("DQ0", False)  # Tank is not filling
    
    # Yield fixture
    yield plc
    
    # Cleanup conditions (if needed)
    await plc.set_object_value("DI8", False)  # Reset overfilling sensor
    # Reset other conditions if needed
    
    # Disconnect
    await plc.disconnect()

# Trigger Tank Level Too High Alarm (A0)
@pytest.mark.asyncio
async def test_tank_level_too_high_alarm(plc: PLCClient):
    await plc.set_object_value("DI8", True)  # Simulate Tank Level Too High condition
    await asyncio.sleep(1)  # Waiting for reactions
    assert await plc.get_object_value("DQ0") == False  # Tank filling should stop
    assert await plc.get_object_value("DQ1") == False  # Tank discharge should stop
    assert await plc.get_object_value("DQ2") == False  # Fluid heating should stop
    assert await plc.get_object_value("DQ3") == True  # Discharge gate should open
    
    assert await plc.get_alarm_status("A0") == True  # Tank Level Too High Alarm is triggered
