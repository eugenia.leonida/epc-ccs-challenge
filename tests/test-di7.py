# -*- coding: utf-8 -*-
"""
Created on Fri Dec 22 16:53:52 2023

@author: eugen
"""

import asyncio
import pytest
import pytest_asyncio

from src.plc_client import PLCClient

SERVER_URL = "opc.tcp://localhost:7000/freeopcua/server/"
CLIENT_TIMEOUT = 5  # seconds

@pytest_asyncio.fixture()
async def plc() -> PLCClient:
    """Instance of the OPC UA client to communicate with the simulator."""
    plc = PLCClient(url=SERVER_URL, timeout=CLIENT_TIMEOUT)
    await plc.init()
    yield plc
    await plc.disconnect()

@pytest.mark.asyncio
async def test_high_level_reached(plc: PLCClient):
    # Simulate reaching the high level
    await plc.set_object_pulse("DI1")  
    await plc.set_object_pulse("DI7")  
    await asyncio.sleep(1)
    
    print("DQ0:", await plc.get_object_value("DQ0"))
    assert await plc.get_object_value("DQ0") == False  # Tank filling stops 
    assert await plc.get_object_value("DQ2") == True  # Tank starts heating
