# -*- coding: utf-8 -*-
"""
Created on Fri Dec 22 14:49:04 2023

@author: eugen
"""

import asyncio
import pytest
import pytest_asyncio

from src.plc_client import PLCClient

SERVER_URL = "opc.tcp://localhost:7000/freeopcua/server/"
CLIENT_TIMEOUT = 5  # seconds

@pytest_asyncio.fixture()
async def plc() -> PLCClient:
    """ Instance of the OPC UA client to communicate with the simulator """
    plc = PLCClient(url=SERVER_URL, timeout=CLIENT_TIMEOUT)
    await plc.init()
    
    # Set initial conditions
    await plc.set_object_value("DQ3", False)  # Tank is not filling
    
    # Yield fixture
    yield plc
    
    # Cleanup conditions (if needed)
    
 
# Trigger Discharging Door Open Alarm (A4)
@pytest.mark.asyncio
async def test_discharging_door_open_alarm(plc: PLCClient):
    await plc.set_object_value("DI9", True)  # Simulate Discharging Door Open condition
    await asyncio.sleep(1)  # Waiting for reactions
    assert await plc.get_object_value("DQ0") == False  # Tank filling should stop
    assert await plc.get_object_value("DQ1") == False  # Tank discharge should stop
    assert await plc.get_object_value("DQ2") == False  # Fluid heating should stop
    assert await plc.get_object_value("DQ3") == False
    assert await plc.get_alarm_status("A4") == True  # Discharging Door Open Alarm is triggered
