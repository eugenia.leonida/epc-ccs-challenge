# TSC2024-1 SY-EPC-CCS

## Challenge
**Evgenia Leonida**

### Control Process Overview
The industrial control process involves a fluid flow system into a tank vessel. The goal is to implement the necessary logic and tests to ensure the proper functioning of the control process. In this .zip file you will find attached the results of this challenge in a .pdf file.

### Filing
Find the code needed for the plc-simulator at src directory and the tests at tests directory. The open exercise is at the OpenExercise folder.

### Key Components and Features
1. **Fluid Flow System:** Liquid enters to be heated, a valve controlled by temperature.
2. **Tank Level Monitoring:** It has 4 states: low-low, low, high, high-high. Low-low and high-high trigger alarms, while low and high are normal operation. The system fills until high and discharges until low.
3. **Temperature Control:** Valve controlled to heat until set point (45 �C).
4. **Tank Discharging Gate:** Open or Closed. If open, it triggers an alarm and shuts down the system; if closed, it is during normal operation.
5. **Operator Controls:** Start, Run, Stop, Emergency, and Reset buttons that initiate the system, run it, or stop/reset it.

| Symbol | Description                      | Type            |
|--------|----------------------------------|-----------------|
| DQ0    | Valve that fills the system      | Digital Output  |
| DQ1    | Valve that empties the system    | Digital Output  |
| DQ2    | Valve controlled to heat         | Digital Output  |
|        | until set point                   |                 |
| DQ3    | Open-closed gate                  | Digital Output  |
| AI0    | Temperature analog input         | Analog Input    |
| DI0    | Start                            | Digital Input   |
| DI1    | Run                              | Digital Input   |
| DI2    | Stop                             | Digital Input   |
| DI3    | Emergency                        | Digital Input   |
| DI4    | Reset                            | Digital Input   |
| DI5    | Low-low                          | Digital Input   |
| DI6    | Low                              | Digital Input   |
| DI7    | High                             | Digital Input   |
| DI8    | High-high                        | Digital Input   |
| DI9    | Gate status                      | Digital Input   |

**Table 1:** List of Symbols and Descriptions for Control System Inputs and Outputs

### Expected Behavior
1. The system starts with the operator command and begins prefilling the tank until the low level if the discharging gate is closed.
2. Upon readiness, the operator runs the system, filling the tank to the high level.
3. Heating commences until the fluid reaches 45 �C.
4. The tank discharges until the low level, and the cycle repeats until the operator stops or errors occur.

### Alarm Definitions
1. **Tank Level Too High Alarm (A0):** The system is at the "High-High," immediately stops to prevent overflow, and it is put to error mode. Requires operator acknowledgment.
2. **Tank Level Too Low Alarm (A1):** The system is at the "Low-Low," immediately stops to prevent a dry tank, and it is put to error mode. Requires operator acknowledgment.
3. **Fluid Temperature Too High Alarm (A2):** If the temperature of the fluid is above 80 �C, the heating system shuts down. Requires operator acknowledgment.
4. **Fluid Temperature Too Low Alarm (A3):** If the temperature of the fluid is below 10 �C, it stops the fluid flow. Requires operator acknowledgment.
5. **Discharging Door Open Alarm (A4):** The system cannot start until the door is closed. Requires operator acknowledgment, and the door has to close before operation.
6. **Emergency Button Pressed Alarm (A5):** Immediate and complete shutdown of the entire system. Requires operator acknowledgment after the emergency.

### GRAFCET Logic Overview
| Step                       | Conditions                                     | Actions                                                                     |
|----------------------------|------------------------------------------------|-----------------------------------------------------------------------------|
| Initialization Step (INIT)  | Reset all variables and flags.                | Ensure all valves are closed. Set the system to the initial state.          |
| Prefilling Step (PREFILL)  | Operator presses the Start button (DI0).      | Open the valve for filling (DQ0). Discharging gate is closed (DI9 is OFF). Monitor the tank level. Transition to Run Step when the low level is reached. |
| Run Step (RUN)              | Operator presses the Run button (DI1).        | Continue filling the tank until the high level is reached. Tank level is low (DI6 is ON). Transition to Tank Filling Step when the high level is reached. |
| Tank Filling Step (FILL)    | Tank level is high (DI7 is ON).               | Close the valve for filling (DQ0). Transition to Temperature Control Step. |
| Temperature Control Step (HEAT) | Tank level is high (DI7 is ON).             | Open the valve for heating (DQ2). Temperature is below 45 �C (AI0 < 45). Monitor the temperature. Transition to Tank Discharging Step when the temperature reaches 45 �C. |
| Tank Discharging Step (DISCHARGE) | Tank level is low (DI6 is ON).           | Close the valve for heating (DQ2). Open the valve for discharging (DQ1). Monitor the tank level. Transition to Prefilling Step when the low level is reached. |
| Stop Step (STOP)            | Operator presses the Stop button (DI2).      | Close all valves. Transition to Initialization Step.                       |
| Emergency Stop Step (ESTOP) | Emergency button pressed (DI3).              | Immediate system shutdown. Open the tank. Full discharge. Transition to Error Mode Step. |
| Error Mode Step (ERROR)     | Any alarm condition is met.                  | Display the corresponding alarm message. Transition to Alarm Acknowledgment Step. |
| Alarm Acknowledgment Step (RESET) | Operator acknowledges the alarm (DI4).  | Acknowledge the alarm. Transition to Initialization Step.                    |

**Table 2:** GRAFCET Logic Overview